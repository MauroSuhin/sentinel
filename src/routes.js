import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import InsertNewTamponTest from './components/InsertNewTamponTest.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/new',
            name: 'InsertNewTamponTest',
            component: InsertNewTamponTest
        },

    ]
})




